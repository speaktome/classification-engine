function [confusion_matrix,PR_matrix,tr_acc,te_acc] = main(nwind,noverlap,mode)
%% Initialization
close all;clc;

%% Setup the parameters 
num_labels = 4;      
                         
%% =========== Read training Data =============

[X,y] = readData('processedData/training_set',nwind,noverlap,num_labels,mode);
disp('dfgdsgbvjbdskno');
length(X)
%% Feature normalization  
[X_norm, mu, sigma] = featureNormalize(X);

MatToArff(X_norm,y,'training_data.arff',mode);

% hold on;
% plot(X_norm);
%% ============ Vectorize Logistic Regression ============

lambda = 0.1;
all_theta = oneVsAll(X_norm, y, num_labels, lambda);

%% =========== Predict for One-Vs-All (Test Set) ==========

pred = predictOneVsAll(all_theta, X_norm);
tr_acc = mean(double(pred == y))*100;
%fprintf('\nTraining Set Accuracy: %f\n', tr_acc * 100);

%% ========== Predict for One-Vs-All (Training Set) ==============

[X2,y2] = readData('processedData/test_set',nwind,noverlap,num_labels,mode);
[X_norm2, mu2, sigma2] = featureNormalize(X2);
MatToArff(X_norm2,y2,'test_data.arff',mode);
pred2 = predictOneVsAll(all_theta, X_norm2);

te_acc = mean(double(pred2 == y2))*100;
%fprintf('\nTest Set Accuracy: %f\n', te_acc * 100);
confusion_matrix = confusionMatrix(y2,pred2);
length_confusion = size(confusion_matrix,2);
PR_matrix = zeros(length_confusion,2);
    for i=1:1:length_confusion
        precision = confusion_matrix(i,i)/sum(confusion_matrix(i,:)) ; 
        recall = confusion_matrix(i,i)/sum(confusion_matrix(:,i));
        PR_matrix(i,1) = precision; 
        PR_matrix(i,2) = recall;
    end
end