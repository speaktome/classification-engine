function MatToArff(X,Y,fName,mode)
    t = size(X);
    fid = fopen(fName,'w');            %# Open the file
    fprintf(fid,'@relation activity\n');
    if (strcmp(mode,'generic' )==1)
     fprintf(fid,'@attribute class {lift, drag, rotate, shaking}\n\n');
    else if (strcmp(mode,'objectspecific' )==1)
     fprintf(fid,'@attribute class {bookopen, bicycle, bottle, cricket}\n\n');
        end
    end
    
       
    for j=1:1:t(2)
       fprintf(fid,'@attribute var_%d numeric\n',j);
    end
    
    fprintf(fid,'\n\n@data\n\n');
    
    for i=1:1:t(1)
    if (strcmp(mode,'generic' )==1)
       switch Y(i)
          case 1
              fprintf(fid,'lift,');
          case 2 
              fprintf(fid,'drag,');
          case 3
              fprintf(fid,'rotate,');
          case 4
              fprintf(fid,'shaking,');
      end
    else if(strcmp(mode,'objectspecific' )==1)
      switch Y(i)
          case 1
              fprintf(fid,'bookopen,');
          case 2 
              fprintf(fid,'bicycle,');
          case 3
              fprintf(fid,'bottle,');
          case 4
              fprintf(fid,'cricket,');
      end
        end
    end

        for k=1:1:t(2)
            if (k==t(2))
                fprintf(fid,'%f\n',X(i,k));
            else
                fprintf(fid,'%f,',X(i,k));
            end
        end
    end
    fclose(fid);
end