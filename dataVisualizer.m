clear all;close all;clc;

[X1 Y1 Z1] = getPlotParameters('processedData\training_set\rotate_3.dat',30);
n = 1:1:length(X1);

width = 7;     % Width in inches
height = 5.8;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 11;      % Fontsize
lw = 1.5;      % LineWidth
msz = 8;       % MarkerSize

figure('Color',[1 1 1]);
pos = get(gcf, 'Position');
set(gcf, 'Position', [0 5 width*100, height*100]); %<- Set size
set(gca, 'FontSize', fsz, 'LineWidth', alw); %<- Set properties

subplot(4,2,1);
plot(n,X1,n,Y1,n,Z1);
title('(a) Rotating')
axis([0 60 -2 2])
xlabel('Sample Number')
ylabel('Acceleration')

[X2 Y2 Z2] = getPlotParameters('processedData\training_set\lift_7.dat',50);
% hFig = figure('Color',[1 1 1]);
% set(hFig, 'Position', [0 100 500 250]);
subplot(4,2,3);
plot(n,X2,n,Y2,n,Z2);
title('(b) Lifting')
axis([0 60 -2 2])
xlabel('Sample Number')
ylabel('Acceleration')

[X3 Y3 Z3] = getPlotParameters('processedData\training_set\drag_7.dat',50);
% hFig = figure('Color',[1 1 1]);
% set(hFig, 'Position', [0 100 500 250]);
subplot(4,2,5);
plot(n,X3,n,Y3,n,Z3);
title('(c) Dragging')
axis([0 60 -2 2])
xlabel('Sample Number')
ylabel('Acceleration')

[X4 Y4 Z4] = getPlotParameters('processedData\training_set\shaking_3.dat',5);
%  hFig = figure('Color',[1 1 1]);
%  set(hFig, 'Position', [0 100 500 250]);
subplot(4,2,7);
plot(n,X4,n,Y4,n,Z4);
title('(d) Shaking')
axis([0 60 -2 2])
xlabel('Sample Number')
ylabel('Acceleration')

%%
[X11 Y11 Z11] = getPlotParameters('processedData\training_set\bookopen_3.dat',15);
% hFig = figure('Color',[1 1 1]);
% set(hFig, 'Position', [0 100 500 250]);
subplot(4,2,2);
plot(n,X11,n,Y11,n,Z11);
title('(e) Opening Book');
axis([0 60 -2 2]);
xlabel('Sample Number')
ylabel('Acceleration')

[X22 Y22 Z22] = getPlotParameters('processedData\training_set\bicycle_7.dat',50);
% hFig = figure('Color',[1 1 1]);
% set(hFig, 'Position', [0 100 500 250]);
subplot(4,2,4);
plot(n,X22,n,Y22,n,Z22);
title('(f) Turning Bicycle')
axis([0 60 -2 2])
xlabel('Sample Number')
ylabel('Acceleration')

[X33 Y33 Z33] = getPlotParameters('processedData\training_set\bottle_7.dat',40);
% hFig = figure('Color',[1 1 1]);
% set(hFig, 'Position', [0 100 500 250]);
subplot(4,2,6);
plot(n,X33,n,Y33,n,Z33);
title('(g) Opening a Bottle Cap')
axis([0 60 -2 2])
xlabel('Sample Number')
ylabel('Acceleration')

[X44 Y44 Z44] = getPlotParameters('processedData\training_set\cricket_3.dat',4);
% hFig = figure('Color',[1 1 1]);
% set(hFig, 'Position', [0 100 500 250]);
subplot(4,2,8);
plot(n,X44,n,Y44,n,Z44);
title('(h) Serve Shot of Tennis Racket')
axis([0 60 -2 2])
xlabel('Sample Number')
ylabel('Acceleration')

% p = 1:1:8;
% accuracies_generic = [77 62 66 84 72 66 69 78];
% accuracies_objectspecific = [84 75 82 93 89 71 71 93];
% figure('Color',[1 1 1]);
% plot(accuracies_generic,'rx');
% hold on;
% plot(accuracies_objectspecific,'o');
% axis([0 10 0 130]);
% legend('Generic activities','Object specific activities')
% xlabel('Algorithm')
% ylabel('Accuracy')


% Here we preserve the size of the image when we save it.
set(gcf,'InvertHardcopy','on');
set(gcf,'PaperUnits', 'inches');
papersize = get(gcf, 'PaperSize');
left = (papersize(1)- width)/2;
bottom = (papersize(2)- height)/2;
myfiguresize = [left, bottom, width, height];
set(gcf,'PaperPosition', myfiguresize);

% Save the file as PNG
print('improvedExample','-dpng','-r300');

