function [X_norm, mu, sigma] = featureNormalize(X)
%FEATURENORMALIZE Normalizes the features in X 
%   FEATURENORMALIZE(X) returns a normalized version of X where
%   the mean value of each feature is 0 and the standard deviation
%   is 1. This is often a good preprocessing step to do when
%   working with learning algorithms.

X_norm =zeros(size(X));
mu = zeros(size(X, 1),1);
sigma = zeros(size(X, 1),1);
  
    for iter = 1:size(X,1)

        mu(iter) = mean(X(iter,:));
        sigma(iter) = std(X(iter,:));
        X_norm(iter,:) = (X(iter,:) - mu(iter))/sigma(iter);
    end
end
