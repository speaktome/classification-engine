function [confusion_matrix,PR_matrix,tr_acc,te_acc] = main(nwind,noverlap)
%% Initialization

[X,y] = readData('processedData/training_set',nwind,noverlap);

%% Feature normalization  
[X_norm, mu, sigma] = featureNormalize(X);

%% Seting up the parameters 
input_layer_size  = size(X,2);  
hidden_layer_size = 25;   % 25 hidden units
num_labels = 4;          % 3 labels, from 1 to 3 
                          % (note that we have mapped "0" to label 10)

%% ================ Initializing Pameters ================

fprintf('\nInitializing Neural Network Parameters ...\n')

initial_Theta1 = randInitializeWeights(input_layer_size, hidden_layer_size);
initial_Theta2 = randInitializeWeights(hidden_layer_size, num_labels);

% Unroll parameters
initial_nn_params = [initial_Theta1(:) ; initial_Theta2(:)];


%% =============== Implement Backpropagation ===============
%
fprintf('\nChecking Backpropagation... \n');

%  Check gradients by running checkNNGradients
%
checkNNGradients;

fprintf('\nProgram paused. Press enter to continue.\n');
pause;


%% =============== Implement Regularization ===============

fprintf('\nChecking Backpropagation (w/ Regularization) ... \n')

%  Check gradients by running checkNNGradients
lambda = 3;
%checkNNGradients(lambda);

% Also output the costFunction debugging values
debug_J  = nnCostFunction(initial_nn_params, input_layer_size,hidden_layer_size, num_labels, X_norm, y, lambda);

fprintf('Program paused. Press enter to continue.\n');
pause;


%% =================== Training Neural network ===================

fprintf('\nTraining Neural Network... \n')

%  After you have completed the assignment, change the MaxIter to a larger
%  value to see how more training helps.
options = optimset('MaxIter', 10000);

%  You should also try different values of lambda
lambda = 1;

% Create "short hand" for the cost function to be minimized
costFunction = @(p) nnCostFunction(p, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, X_norm, y, lambda);

% Now, costFunction is a function that takes in only one argument (the
% neural network parameters)
[nn_params, cost] = fmincg(costFunction, initial_nn_params, options);

% Obtain Theta1 and Theta2 back from nn_params
Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1));

Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1));

fprintf('Program paused. Press enter to continue.\n');
pause;

%% ================= Implement Predict =================

pred = predict_nn(Theta1, Theta2, X_norm);
tr_acc = mean(double(pred == y))*100;
%fprintf('\nTraining Set Accuracy: %f\n', mean(double(pred == y)) * 100);

[X2,y2] = readData('processedData/test_set',nwind,noverlap);
[X_norm2, mu2, sigma2] = featureNormalize(X2);

pred2 = predict_nn(Theta1, Theta2, X_norm2);

te_acc = mean(double(pred2 == y2))*100;

%fprintf('\nTest Set Accuracy: %f\n', mean(double(pred2 == y2)) * 100);
confusion_matrix = confusionMatrix(y2,pred2);
length_confusion = size(confusion_matrix,2);
PR_matrix = zeros(length_confusion,2);
    for i=1:1:length_confusion
        precision = confusion_matrix(i,i)/sum(confusion_matrix(i,:)) ; 
        recall = confusion_matrix(i,i)/sum(confusion_matrix(:,i));
        PR_matrix(i,1) = precision; 
        PR_matrix(i,2) = recall;
    end
end
