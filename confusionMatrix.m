function confusion_mat = confisionMatrix(X,Y)
%%
%       Declare H1 Declare H2 Declare H3
% Is H1   --         --         --
% Is H2   --         --         --
% Is H3   --         --         --
%%
n_classes = length(unique(X));
confusion_mat = zeros(n_classes,n_classes);

    for i=1:1:n_classes
        index = find(X == i);
        temp = Y(index);
        for j= 1:1:n_classes
            confusion_mat(i,j) = sum((temp == j));
        end         
        confusion_mat(i,:) = (confusion_mat(i,:)./ sum(confusion_mat(i,:))) * 100;    
    end
end 





