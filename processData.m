%%usage processData('processedData\rawdata\*.dat')
function[] = processData(dirName)
    fileList = dir(dirName);
    for i=1:size(fileList)
        cvsFileName_Ext = fileList(i).name;
        c = textscan(cvsFileName_Ext, '%s', 'delimiter', '.');
        cvsFileName = c{1}{1};
        csvFileID = fopen(['processedData\rawdata\' cvsFileName_Ext], 'r')
        datFileID = fopen(['processedData\processed_all\' cvsFileName '.dat'], 'w')
        
        nextline = fgets(csvFileID);
        lineNo = 1;
        while ischar(nextline)
            if(lineNo >= 10)
                fprintf(datFileID, nextline);
            end
            nextline = fgets(csvFileID);
            lineNo = lineNo + 1;            
        end
        fclose(csvFileID);
        fclose(datFileID);
    end
end