function features = featureExtraction(x,nwind,noverlap)

      % Reading the data set of the selected file     
       X1 = x(:,1).*1000;  Y1 = x(:,2).*1000; Z1= x(:,3).*1000;  
       nx = length(X1);
       gravity = zeros(1,3);
       %%Remove gravity component
       for j=1:1:nx
         alpha2 =  0.8;
         gravity(1) = alpha2 * gravity(1) + (1 - alpha2) * X1(j);
         gravity(2) = alpha2 * gravity(2) + (1 - alpha2) * Y1(j);
	     gravity(3) = alpha2 * gravity(3) + (1 - alpha2) * Z1(j);
         X1(j) = X1(j) - gravity(1);
         Y1(j) = Y1(j) - gravity(2);
         Z1(j) = Z1(j) - gravity(3);
       end
       X1 = X1(20:nx);
       Y1 = Y1(20:nx);
       Z1 = Z1(20:nx);
       nx = length(X1);
      %% feature extraction 
      % features : mean,standart deviation , energy , correlation
             
        %% Sliding window algorithm
       
        idx = bsxfun(@plus, (1:nwind)', 1+(0:(fix((nx-noverlap)/(nwind-noverlap))-1))*(nwind-noverlap))-1;
         
        nFeatures = 12;
        features = zeros(1,size(idx,2)*nFeatures);
        windowFeatures = zeros(1,nFeatures);
        % loop over sliding windows and extract features
        t=1;
        for k=1:size(idx,2)
            slidingWindowX1 = X1(idx(:,k));
            slidingWindowY1 = Y1(idx(:,k));
            slidingWindowZ1 = Z1(idx(:,k));
            windowFeatures(1) = mean(slidingWindowX1);
            windowFeatures(2) = var(slidingWindowX1);
            windowFeatures(3) = mean(slidingWindowY1);
            windowFeatures(4) = var(slidingWindowY1);           
            windowFeatures(5) = mean(slidingWindowZ1);
            windowFeatures(6) = var(slidingWindowZ1);
           
            windowFeatures(7) = sqrt(sum(slidingWindowX1.^2)+sum(slidingWindowY1.^2)+sum(slidingWindowZ1.^2));
            % SMA - Signal magnitude area
            %windowFeatures(8) = (sum(abs(slidingWindowX1)) + sum(abs(slidingWindowY1))+sum(abs(slidingWindowZ1)))/nwind;
            windowFeatures(8) = (sum((slidingWindowX1)) + sum((slidingWindowY1))+sum((slidingWindowZ1)))/nwind;
            windowFeatures(9) = (countLevelCrossing(slidingWindowX1',0)+countLevelCrossing(slidingWindowY1',0)+countLevelCrossing(slidingWindowZ1',0))/3*nx;
            windowFeatures(10) = (countLevelCrossing(slidingWindowX1',mean(slidingWindowX1))+countLevelCrossing(slidingWindowY1',mean(slidingWindowY1))+countLevelCrossing(slidingWindowZ1',mean(slidingWindowZ1)))/3*nx;
            corr_xy = corrcoef(slidingWindowX1,slidingWindowY1);
            corr_yz = corrcoef(slidingWindowZ1,slidingWindowY1);
            windowFeatures(11) = corr_xy(1,2);
            windowFeatures(12) = corr_yz(1,2);
            features(t:t+nFeatures-1) = windowFeatures;
            t = t+nFeatures;
        end
end
