function [X1,Y1,Z1] = getPlotParameters(fileName,scaleFactor)
x = csvread(fileName);  
% figure;
% plot(x);
%X1 = x(:,1).*40;  Y1 = x(:,2).*40; Z1= x(:,3).*40;  
X1 = x(:,1).*scaleFactor;  Y1 = x(:,2).*scaleFactor; Z1= x(:,3).*scaleFactor;  
nx = length(X1);
gravity = zeros(1,3);
for j=1:1:nx
     alpha2 =  0.8;
     gravity(1) = alpha2 * gravity(1) + (1 - alpha2) * X1(j);
     gravity(2) = alpha2 * gravity(2) + (1 - alpha2) * Y1(j);
     gravity(3) = alpha2 * gravity(3) + (1 - alpha2) * Z1(j);
     X1(j) = X1(j) - gravity(1);
     Y1(j) = Y1(j) - gravity(2);
     Z1(j) = Z1(j) - gravity(3);
end
X1 = X1(20:nx);
Y1 = Y1(20:nx);
Z1 = Z1(20:nx);
end