function [crossing] = countLevelCrossing(data, level)

n = length(data);
crossing = 0;

for i=1:1:(n-1)
    if((data(i)>level && data(i+1)<level)|| (data(i)<level && data(i+1)>level))
        crossing = crossing+1;
    end
end

end