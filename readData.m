function [X,Y] = readData(dirname,nwind,noverlap,num_labels,mode)
  file_list = dir([dirname '/*.dat']);
  
  z = size(file_list);
  X = [];
  Y = zeros(z,1);
  
  for i=1:1:z
      file_name = file_list(i).name;   
      
      c = textscan(file_name, '%s', 'delimiter', '_'); 
      interaction_type = c{1}{1};
      
      file_name = strcat(dirname,'/',file_name); 
      x = csvread(file_name);    
     
      X(i,:) = featureExtraction(x,nwind,noverlap);      
    
  if (strcmp(mode,'generic')==1)
       switch interaction_type
          case 'lift'
              Y(i,1) = 1;
          case 'drag'
              Y(i,1) = 2;
          case 'rotate'
              Y(i,1) = 3;
          case 'shaking'
              Y(i,1) = 4;
      end
    else if(strcmp(mode,'objectspecific' )==1)
      switch interaction_type
          case 'bookopen'
            Y(i,1) = 1;
          case 'bicycle'
            Y(i,1) = 2; 
          case 'bottle'
            Y(i,1) = 3; 
          case 'cricket'
            Y(i,1) = 4; 
      end
        end
  end
  end
end