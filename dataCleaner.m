%%
% This is used to convert raw .lgdat files produces by the data collection
% server to a readable format to MATLAB
%%
files=dir('processedData/rawdata/*.lgdat');
for i=1:length(files)
    filename=files(i).name;
    [pathstr, name, ext] = fileparts(filename)
    copyfile(['processedData/rawdata/' filename], fullfile('', [name '.dat']));
    
 fid = fopen([name '.dat'], 'r') ;             % Open source file.
 fgetl(fid) ;                                  % Read/discard line.
 buffer = fread(fid, Inf) ;                    % Read rest of the file.
 fclose(fid);
 delete [name '.dat'];
 
 fid = fopen([name '.dat'], 'w');   % Open destination file.
 fwrite(fid, buffer) ;                         % Save to file.
 fclose(fid) ;
 movefile([name '.dat'], 'processedData/rawdata/');
end

dirName = 'processedData\rawdata\*.dat';
fileList = dir(dirName);

for i=1:size(fileList)
    cvsFileName_Ext = fileList(i).name
    c = textscan(cvsFileName_Ext, '%s', 'delimiter', '.');
    cvsFileName = c{1}{1};
    csvFileID = fopen(['processedData\rawdata\' cvsFileName_Ext], 'r')
    datFileID = fopen(['processedData\processed_all\' cvsFileName '.dat'], 'w')

    nextline = fgets(csvFileID);
    lineNo = 1;
    while ischar(nextline)
        if(lineNo >= 10)
            fprintf(datFileID, nextline);
        end
        nextline = fgets(csvFileID);
        lineNo = lineNo + 1;            
    end
    fclose(csvFileID);
    fclose(datFileID);
end